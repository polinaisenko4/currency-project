import React, {
  createContext,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState,
} from "react";
import { getLatest } from "../api";

const CurrencyContext = createContext({
  currency: null,
  exchange: () => {},
  currencyCodes: [],
});

export const useCurrency = () => {
  return useContext(CurrencyContext);
};

const CurrencyProvider = ({ children }) => {
  const [currency, setCurrency] = useState(null);

  const exchange = useCallback(
    (amount, currency1, currency2) => {
      if (!currency) {
        return 0;
      }
      if (currency1 === "USD") {
        return (currency.rates[currency2] * amount).toFixed(2);
      } else {
        const currencyUSD = currency.rates[currency1];
        return ((currency.rates[currency2] * amount) / currencyUSD).toFixed(2);
      }
    },
    [currency]
  );

  const currencyCodes = useMemo(() => {
    if (currency) {
      return [...Object.keys(currency.rates)].sort();
    } else {
      return [];
    }
  }, [currency]);

  useEffect(() => {
    getLatest().then(setCurrency);
  }, []);

  return (
    <CurrencyContext.Provider value={{ currency, exchange, currencyCodes }}>
      {children}
    </CurrencyContext.Provider>
  );
};

export default CurrencyProvider;
