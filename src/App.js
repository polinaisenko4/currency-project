import "./App.css";
import Header from "./components/Header/Header";
import MainPage from "./components/MainPage/MainPage";
import CurrencyProvider from "./providers/Currency.provider";

function App() {
  return (
    <CurrencyProvider>
      <div className="App">
        <Header />
        <MainPage />
      </div>
    </CurrencyProvider>
  );
}

export default App;
