import React from "react";
import "./Header.css";
import { useCurrency } from "../../providers/Currency.provider";

const Header = () => {
  const { exchange, currency } = useCurrency();

  if (!currency) {
    return <div>Loading...</div>;
  }

  return (
    <div className="header">
      <div className="header-currency">
        <p>USD/UAH {exchange(1, "USD", "UAH")}</p>
        <p>EUR/UAH {exchange(1, "EUR", "UAH")}</p>
      </div>
    </div>
  );
};

export default Header;
