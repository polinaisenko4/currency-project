import './MainPage.css'

import { useState } from "react";
import { useCurrency } from "../../providers/Currency.provider";
import Currency from "../Currency/Currency";

function MainPage() {

  const {exchange} = useCurrency()

  const [currencyInput1, setCurrencyInput1] = useState(0);
  const [currencySelect1, setCurrencySelect1] = useState("USD");
  const [currencyInput2, setCurrencyInput2] = useState(0);
  const [currencySelect2, setCurrencySelect2] = useState("UAH");

  const onChangeInput1 = (value) => {
    setCurrencyInput1(value)
    setCurrencyInput2(exchange(value, currencySelect1, currencySelect2))
  }

  const onChangeSelect1 = (value) => {
    setCurrencySelect1(value)
    setCurrencyInput2(exchange(currencyInput1, value, currencySelect2))
  }

  const onChangeInput2 = (value) => {
    setCurrencyInput2(value)
    setCurrencyInput1(exchange(value, currencySelect2, currencySelect1))
  }

  const onChangeSelect2 = (value) => {
    setCurrencySelect2(value)
    setCurrencyInput1(exchange(currencyInput2, currencySelect2, currencySelect1))
  }

  return (
      <div>
        <Currency
          value={currencyInput1}
          onChange={onChangeInput1}
          currencyCode={currencySelect1}
          onChangeCurrencyCode={onChangeSelect1}
        />
        <Currency
          value={currencyInput2}
          onChange={onChangeInput2}
          currencyCode={currencySelect2}
          onChangeCurrencyCode={onChangeSelect2}
        />
      </div>
  
  );
}

export default MainPage;
