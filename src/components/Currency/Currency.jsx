import React from "react";
import { useCurrency } from "../../providers/Currency.provider";

import { MenuItem, Select, TextField } from "@mui/material";

function Currency(props) {
  const { currency, currencyCodes } = useCurrency();

  console.log(currency?.rates);

  if (!currency) {
    return <div>Loading...</div>;
  }

  return (
    <div>
      
      <TextField
        value={props.value}
        onChange={(event) => {
          props.onChange(event.target.value);
        }}
      />
      <Select
        value={props.currencyCode}
        onChange={(event) => {
          props.onChangeCurrencyCode(event.target.value);
        }}
      >
        {currencyCodes.map((el, i) => {
          return (
            <MenuItem key={i} value={el}>
              {el}
            </MenuItem>
          );
        })}
      </Select>
    </div>
  );
}

export default Currency;
