import axios from "axios";

const api = axios.create({
  baseURL: "https://openexchangerates.org/api/",
  headers: {
    Authorization: `Token ${process.env.REACT_APP_APP_ID}`,
  },
});

export const getLatest = async () => {
  try {
    const result = await api.get(`/latest.json`);
    return result.data;
  } catch (error) {
    console.log("Ошибка: " + error);
  }
};
